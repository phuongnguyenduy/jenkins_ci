package demo.locators;

public class Iboard_upcom_locators {
    public static final String O_DANH_MUC = ".non-link";
    public static final String INPUT_DANH_MUC = "//input[@id='watchlist']";
    public static final String ADD_CATEGORY_BTN = ".watchlist-input>form>button";
    public static final String CATEGORY_LIST = ".text-watchlist";
    public static final String STOCK_SYMBOL = "//td[contains(@class,'stockSymbol delete-btn txt-up')]";
    public static final String REMOVE_STOCK_BTN = ".remove-stock";
    public static final String ADDED_STOCK = "body.sidebar-fixed.sidebar-widget-fixed.sidebar-widget-hidden.sidebar-hidden.aside-menu-hidden.alertbar-hidden.dark-body.ReactModal__Body--open:nth-child(2) > script:nth-child(5)";
    public static final String TICKER_SYMBOL_INP = ".stock-search-price-board #search-price-board";
    public static final String NOTIFICATION = ".notification-container >span";
    public static final String ADDED_CATEGORY_ON_LIST = "//div[@class='text-watchlist']";
    public static final String ADDED_CATEGORY_AFTER_CLICK = "//a[@class='btn-exchange btn-exchange-active']";
}
