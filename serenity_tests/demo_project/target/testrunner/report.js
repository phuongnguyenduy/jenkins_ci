$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/danh_mục/tạo_danh_mục/tạo_danh_mục.feature");
formatter.feature({
  "name": "Tạo danh mục",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Tạo mới danh mục thành công",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@TC01"
    }
  ]
});
formatter.step({
  "name": "Danh mục \"\u003ctên_mục\u003e\" được tạo thành công",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "tên_mục"
      ]
    },
    {
      "cells": [
        "phuongtest"
      ]
    }
  ]
});
formatter.background({
  "name": "Khách hàng đã đăng nhập thành công",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "Khách hàng truy cập iboard web",
  "keyword": "Given "
});
